Connect4
========

This is a ready to host "Connect Four" web game written in Python with Flask!

You can play it on the same device or on two different devices.

How to play
-----------

Launch the game with:

```python3 app.py```

"Flask" and "Flask-SQLAlchemy" are required. You can install them with:

```pip3 install Flask```

and

```pip3 install Flask-SQLAlchemy```

Then open your browser and enter:

	localhost:5000/new/game/

You can also change the port to 80 *(see comment at the end in app.py)*
