#Connect4 V4.0
#07.02.19
#Mo.Bit
from flask import Flask, render_template, url_for, request, jsonify
from flask_sqlalchemy import SQLAlchemy
from datetime import datetime
import pygal
from pygal.style import Style
from time import sleep

app = Flask(__name__)
app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///data.db"
db = SQLAlchemy(app)

class Player(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), unique=True, nullable=False)
    win_games = db.relationship("Game", backref="winner", lazy=True)

    def __repr__(self):
        return f"P( {self.name} )"

class Game(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    datetime = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    p1 = db.Column(db.String(35), nullable=False)
    p2 = db.Column(db.String(35), nullable=False)
    board = db.Column(db.String(100), nullable=False)
    winname = db.Column(db.String(35), nullable=False)
    winid = db.Column(db.Integer, db.ForeignKey("player.id"), nullable=False)

    def __repr__(self):
        return f"GM( {self.p1} - {self.p2} : W_ {self.winname} _W )"

class Online(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    c_name = db.Column(db.String(35), nullable=False)
    c_color = db.Column(db.String(6), nullable=False)
    j_name = db.Column(db.String(35), nullable=False)
    j_color = db.Column(db.String(6), nullable=False)
    turn = db.Column(db.Integer, nullable=False)
    board = db.Column(db.String(100), nullable=False)
    wait = db.Column(db.Integer, nullable=False) #1 for waiting, 2 for playing

    def __repr__(self):
        return f"Online( C: {self.c_name} {self.c_color} - J: {self.j_name} {self.j_color} - {self.board} )"

#db.create_all()
#db.drop_all()
#Class.query.all() / query.filter_by(name="").first()

#############################################

def new_board():
    return [
        ["1","2","3","4","5","6","7"],
        ["_","_","_","_","_","_","_"],
        ["_","_","_","_","_","_","_"],
        ["_","_","_","_","_","_","_"],
        ["_","_","_","_","_","_","_"],
        ["_","_","_","_","_","_","_"],
        ["_","_","_","_","_","_","_"]]

def wtest(x,y,board_in, ps): #ex:(3,3) #ps:player's color (symbol) -> (r/y)
    #all 13 win situations
    if check(x+1,x+2,x+3,y,y,y,board_in,ps): #1 for:(4,3)(5,3)(6,3)
        return True
    elif check(x,x,x,y-1,y-2,y-3,board_in,ps): #2 for:(3,2)(3,1)(3,0)
        return True
    elif check(x,x,x,y+1,y+2,y+3,board_in,ps): #3 for:(3,4)(3,5)(3,6)
        return True
    elif check(x-1,x-2,x-3,y-1,y-2,y-3,board_in,ps): #4 for:(2,2)(1,1)(0,0)
        return True
    elif check(x+1,x+2,x+3,y-1,y-2,y-3,board_in,ps): #5 for:(4,2)(5,1)(6,0)
        return True
    elif check(x-1,x-2,x-3,y+1,y+2,y+3,board_in,ps): #6 for:(2,4)(1,5)(0,6)
        return True
    elif check(x+1,x+2,x+3,y+1,y+2,y+3,board_in,ps): #7 for:(4,4)(5,5)(6,6)
        return True
    elif check(x+1,x-1,x-2,y-1,y+1,y+2,board_in,ps): #8 for:(4,2)(2,4)(1,5)
        return True
    elif check(x+1,x-1,x+2,y-1,y+1,y-2,board_in,ps): #9 for:(4,2)(2,4)(5,1)
        return True
    elif check(x+1,x-1,x-2,y+1,y-1,y-2,board_in,ps): #10 for:(4,4)(2,2)(1,1)
        return True
    elif check(x-1,x+1,x+2,y-1,y+1,y+2,board_in,ps): #11 for:(2,2)(4,4)(5,5)
        return True
    elif check(x,x,x,y-1,y+1,y+2,board_in,ps): #12 for:(3,2)(3,4)(3,5)
        return True
    elif check(x,x,x,y+1,y-1,y-2,board_in,ps): #13 for:(3,4)(3,2)(3,1)
        return True
    else:
        return False

def check(x1,x2,x3,y1,y2,y3,board_in, ps): #symbol there or not
    if x_y_check(x1,x2,x3,y1,y2,y3):
        if board_in[x1][y1]==ps and board_in[x2][y2]==ps and board_in[x3][y3]==ps: #-> 4 ps (player's symbol)
            return True
        else:
            return False #no 4 ps
    else:
        return False #out of board

def x_y_check(x1,x2,x3,y1,y2,y3): #only numbers x(1-6) y(0-6) -> (in the board or not)
    if x1<1 or x1>6 or x2<1 or x2>6 or x3<1 or x3>6 or y1<0 or y1>6 or y2<0 or y2>6 or y3<0 or y3>6:
        return False
    else:
        return True

def transfer(board):
    transfer_b=""
    for column in board:
        for row in column:
            transfer_b+=row+" "
    return transfer_b

def statistics(player):
    #pie
    ln = player[0].lower() #winner's name in lower()
    games = len(Game.query.filter_by(p1=ln).all()) + len(Game.query.filter_by(p2=ln).all())
    times_won = len(Player.query.filter_by(name=ln).first().win_games)
    colors= Style(
    colors=("#0099cc","#990000")
    )
    pie = pygal.Pie(half_pie=True, style=colors)
    pie.title = '%s' %(player[0])
    times_won_per = round(times_won/games * 100, 1)
    pie.add('Won', times_won_per)
    pie.add('Lost', 100 - times_won_per)
    pie = pie.render_data_uri()

    #stackedline
    line=pygal.StackedLine(fill=True)
    line.title="History (%s)" %(player[0])
    line.x_title="Games"
    line.x_labels = map(str, range(games +1))
    all_games=Game.query.all()
    history=[0]
    for g in all_games:
        if g.p1 == ln or g.p2 == ln:
            if g.winner.name == ln:
                history.append(1)
            else:
                history.append(-1)
    line.y_labels = [
      {'label': 'WON', 'value': 1},
      {'label': 'LOST', 'value': -1}]
    line.add("History", history)
    line = line.render_data_uri()
    return (pie, line)
#############################################

@app.route("/new/game/")
def new_game():
    return render_template("new_game.html", title="NEW GAME", error= None)

@app.route("/play", methods = ["POST"])
def play():
    print("NEW GAME!")
    p1c=request.form["p1c"] #take player's info for the first time from new_game.html
    p1=[request.form["p1n"],p1c]
    if p1c=="red":
        p2c="yellow"
    else:
        p2c="red"
    p2=[request.form["p2n"],p2c]

    if len(p1[0]) == 0 or len(p2[0]) == 0 or len(p1[0])>35 or len(p2[0])>35: #if no valid name_input
        return render_template("new_game.html", title="NEW GAME", error="No valid input!")
    print()
    print(p1,p2)
    print()
    b= new_board()
    transfer_b=transfer(b)
    return render_template("play.html", b=b, pt=1, p1=p1, p2=p2, transfer_b=transfer_b, error=None)

@app.route("/", methods = ["POST"])
def index():
    #else: x = request.args.get("x") -> same
    try:
        g_id=int(request.form["g_id"])
        multi=True #multi_online
    except:
        multi=False #no multi_online

    clmn=request.form["column"]
    clmn= int(clmn)-1 #-1 for index
    pt=int(request.form["pt"]) #take player's turn
    p1=[request.form["p1n"],request.form["p1c"]] #take player's info from play.html
    p2=[request.form["p2n"],request.form["p2c"]]

    #translate
    transfer_b=request.form["transfer_b"]
    tb=transfer_b.split()
    b=[[],[],[],[],[],[],[]]
    for index,column in enumerate(b):
        for i in range(7):
            b[index].append(tb[i+index*7])

    row = 6
    while row>0: #searching for empty blocks
        if b[row][clmn]=="_":
            win=0
            winner=None
            if pt == 1: #mod board + change turn + check if win
                if p1[1]=="red":
                    b[row][clmn]="r"
                    if wtest(row,clmn,b,"r"):
                        win = 1 #pl1
                        winner = p1
                else:
                    b[row][clmn]="y"
                    if wtest(row,clmn,b,"y"):
                        win = 1
                pt=2
            else:
                if p2[1]=="red":
                    b[row][clmn]="r"
                    if wtest(row,clmn,b,"r"):
                        win = 2 #pl2
                else:
                    b[row][clmn]="y"
                    if wtest(row,clmn,b,"y"):
                        win = 2
                pt=1

            transfer_b=transfer(b)
            if win==1 or win==2:
                #save players
                old_p1=Player.query.filter_by(name=p1[0].lower()).first()
                old_p2=Player.query.filter_by(name=p2[0].lower()).first()
                try: #old player
                    op1n=old_p1.name #no old player -> old_p1 = None -> old_p1.name -> ERORR
                    save_p1=old_p1
                except: #new player
                    save_p1=Player(name=p1[0].lower())
                    db.session.add(save_p1)
                    db.session.commit()
                try:
                    op2n=old_p2.name
                    save_p2=old_p2
                except: #new player
                    save_p2=Player(name=p2[0].lower())
                    db.session.add(save_p2)
                    db.session.commit()

                if win==1:
                    winner=p1
                    loser=p2 #for statistics
                    save_game=Game(p1=p1[0].lower(),p2=p2[0].lower(),
                            board=transfer_b,winname=p1[0].lower(),winid=save_p1.id) #save game
                    if multi==True:
                        turn=3 #3 for p1 (multi_online)
                else:
                    winner=p2
                    loser=p1  #for statistics
                    save_game=Game(p1=p1[0].lower(),p2=p2[0].lower(),
                            board=transfer_b,winname=p2[0].lower(),winid=save_p2.id) #save game
                    if multi==True:
                        turn=4 #4 for p2

                db.session.add(save_game)
                db.session.commit()
                ##################################################
                print()
                for column in b:
                    print (" ".join(column))
                print()
                print("The winner:")
                print(winner)
                print()
                ##################################################
                # statistics
                statistics_winner=statistics(winner)
                statistics_loser=statistics(loser)

                if multi==True:
                    Online.query.get(g_id).turn=turn #win: 3 for p1 - 4 for p2
                    Online.query.get(g_id).board=transfer_b
                    db.session.commit()

                return render_template("winner.html",title="WINNER!",b=b,winner=winner,loser=loser,
                                        pie_winner=statistics_winner[0],line_winner=statistics_winner[1],
                                        pie_loser=statistics_loser[0],line_loser=statistics_loser[1])
            else: #no win -> continue

                if multi==True:
                    if pt==1: #after switch
                        Online.query.get(g_id).turn=1
                        Online.query.get(g_id).board=transfer_b
                        db.session.commit()
                        return render_template("wait_j.html", b=b,g_id=g_id)
                    else:
                        Online.query.get(g_id).turn=2
                        Online.query.get(g_id).board=transfer_b
                        db.session.commit()
                        return render_template("wait_c.html", b=b,g_id=g_id)

                else: #no multi_online
                    return render_template("play.html",b=b,pt=pt,p1=p1,p2=p2,transfer_b=transfer_b,error=None)
        else:
            row-=1
    else:
        if multi==True:
            return render_template("play_online.html", b=b,turn=pt,
                                    c_name=p1[0],c_color=p1[1],
                                    j_name=p2[0],j_color=p2[1],
                                    g_id=g_id,transfer_b=transfer(b), error="Column is full!")
        else: #no multi_online
            return render_template("play.html",b=b,pt=pt,p1=p1,p2=p2,transfer_b=transfer_b,error="Column is full!")

#############################################

@app.route("/multi_online", methods = ["POST"])
def multi_online():
    online_name = request.form["online_name"]
    if len(online_name)==0 or len(online_name)>35:
        return render_template("new_game.html", title="NEW GAME", error="No valid input!")

    if request.form["bt"]=="New":
        new=Online(c_name=online_name,c_color=request.form["c_color"],j_name="notyet", j_color="notyet",
                turn=2,board="notyet", wait=1)
        db.session.add(new)
        db.session.commit()
        return render_template("multi_online_new.html",new=new,online_games=Online.query.all())
    else:
        j_name=request.form["online_name"]
        if len(j_name)==0 or len(j_name)>35:
            return render_template("new_game.html", title="NEW GAME", error="No valid input!")
        return render_template("multi_online_join.html",online_games=Online.query.all(),j_name=j_name,
                                message="Join the game with the number from your friend!")

@app.route("/_wait_c_new", methods=["GET"])
def wait_c_new(): #waiting till the 2. player joins -> cstart
    g_id=int(request.args.get("g_id"))
    limit=160
    while limit>0:
        if Online.query.get(g_id).wait==2:
            #for the GET method
            return jsonify(result="<a class='btn btn-primary' href='/to_wait_c?g_id="+
                            str(g_id)+"' role='button'>Start the game!</a>")
        else:
            limit-=1
            sleep(3)
    else:
        Online.query.get(g_id).wait=2
        db.session.commit()
        return jsonify(result="<p>Time limit reached. Reload the page to continue!</p>")

@app.route("/to_wait_c", methods=["GET"])
def to_wait_c():
    g_id=int(request.args.get("g_id"))
    b=new_board()
    return render_template("wait_c.html", b=b,g_id=g_id)

@app.route("/play_online", methods = ["POST","GET"])
def play_online():
    if request.method == "POST": #for the first time from j
        game_id=request.form["game_id"]
        try:
            game_id=int(game_id)
            online_game=Online.query.get(game_id)
            online_game.wait=2
            online_game.j_name=request.form["j_name"]
            if online_game.c_color=="red":
                online_game.j_color="yellow"
            else:
                online_game.j_color="red"
            db.session.commit()
            b=new_board()
            return render_template("play_online.html", b=b,turn=2,
                                    c_name=online_game.c_name,c_color=online_game.c_color,
                                    j_name=online_game.j_name,j_color=online_game.j_color,
                                    g_id=game_id,transfer_b=transfer(b), error=None)
        except:
            return render_template("multi_online_join.html",online_games=Online.query.all(),
                                    message="Invalid input! Retry joining the game with the number from your friend!")
    else: #after wait_j
        game_id=int(request.args.get("g_id"))
        online_game=Online.query.get(game_id)
        #translate
        tb=online_game.board.split()
        b=[[],[],[],[],[],[],[]]
        for index,column in enumerate(b):
            for i in range(7):
                b[index].append(tb[i+index*7])

        return render_template("play_online.html", b=b,turn=online_game.turn,
                                c_name=online_game.c_name,c_color=online_game.c_color,
                                j_name=online_game.j_name,j_color=online_game.j_color,
                                g_id=game_id,transfer_b=transfer(b), error=None)

@app.route("/_wait_j/", methods=["GET"])
def wait_j(): #waiting till the 2. player plays
    g_id=int(request.args.get("g_id"))
    limit=600
    while limit>0:
        if Online.query.get(g_id).turn==2:
            #for the GET method
            return jsonify(result="<a class='btn btn-primary' href='/play_online?g_id="+
                            str(g_id)+"' role='button'>Your turn!</a>")
        elif Online.query.get(g_id).turn==3:
            return jsonify(result="<a class='btn btn-primary' href='/to_winner?g_id="+
                                str(g_id)+"&w=c' role='button'>Your friend won the game! Press to see statistics</a>")
        else:
            limit-=1
            sleep(0.5)
    else:
        return jsonify(result="<p>Time limit reached. Reload the page to continue!</p>")

@app.route("/_wait_c/", methods=["GET"])
def wait_c(): #waiting till the 2. player plays
    g_id=int(request.args.get("g_id"))
    limit=600
    while limit>0:
        if Online.query.get(g_id).turn==1:
            #for the GET method
            return jsonify(result="<a class='btn btn-primary' href='/play_online?g_id="+
                            str(g_id)+"' role='button'>Your turn!</a>")
        elif Online.query.get(g_id).turn==4:
            return jsonify(result="<a class='btn btn-primary' href='/to_winner?g_id="+
                                str(g_id)+"&w=j' role='button'>Your friend won the game! Press to see statistics</a>")
        else:
            limit-=1
            sleep(0.5)
    else:
        return jsonify(result="<p>Time limit reached. Reload the page to continue!</p>")

@app.route("/to_winner", methods=["GET"])
def to_winner():
    g_id=int(request.args.get("g_id"))
    if request.args.get("w")=="c":
        winner=[Online.query.get(g_id).c_name,Online.query.get(g_id).c_color]
        loser=[Online.query.get(g_id).j_name]
    elif request.args.get("w")=="j":
        winner=[Online.query.get(g_id).j_name,Online.query.get(g_id).j_color]
        loser=[Online.query.get(g_id).c_name]
    #translate
    transfer_b=Online.query.get(g_id).board
    tb=transfer_b.split()
    b=[[],[],[],[],[],[],[]]
    for index,column in enumerate(b):
        for i in range(7):
            b[index].append(tb[i+index*7])

    statistics_winner=statistics(winner)
    statistics_loser=statistics(loser)
    return render_template("winner.html",b=b,winner=winner,loser=loser,
                            pie_winner=statistics_winner[0],line_winner=statistics_winner[1],
                            pie_loser=statistics_loser[0],line_loser=statistics_loser[1])

#############################################

@app.errorhandler(404)
def page_not_found(e):
    return render_template("page_not_found.html")

if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0')

# You can change the port to 80 by adding the arg port="80" like this:
#if __name__ == "__main__":
#    app.run(debug=True, host='0.0.0.0', port="80")
